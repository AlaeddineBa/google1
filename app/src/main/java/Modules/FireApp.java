package Modules;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kabir on 21/03/17.
 */

public class FireApp {

    FirebaseDatabase database;
    DatabaseReference myRef;
    List<SuperMarket> s;

    String result = "";
    public FireApp(){
         database = FirebaseDatabase.getInstance();


    }


    public void addRecipeAndIngredients(String a , String b){


        myRef = database.getReference("recipe").child("aaaaa");
        myRef.setValue("dddd");
    }

    public String getRecipes(){


        myRef = database.getReference("recipes");

        myRef.addValueEventListener(new ValueEventListener() {


            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                Iterable<DataSnapshot> children = dataSnapshot.getChildren();
                int i=0;
                if (children.iterator().hasNext())
                {
                    result += "{recipces:[";

                }
                for (DataSnapshot child : children){


                    result += "{id:'"+i+"',informations:[";
                    Iterable<DataSnapshot> child2 = child.getChildren();

                    for(DataSnapshot child3:child2){
                        String key = child3.getKey();
                        String val = child3.getValue(String.class);
                        result += "{"+key+":"+val+"},";

                    }
                    result=result.substring(0,result.length()-1);

                    result +="]},";

                    i++;


                }
                result=result.substring(0,result.length()-1);

                if(!result.equals(""))
                {
                    result +="]}";
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        Log.v("voilaaa----->",result);

        return result;

    }
    public List<SuperMarket> getSupermarkets(){

        DatabaseReference f;
        f = database.getReference("supermarkets");

        f.addValueEventListener(new ValueEventListener() {


            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                s = new ArrayList<SuperMarket>();

                Iterable<DataSnapshot> children = dataSnapshot.getChildren();



                for (DataSnapshot child : children){


                    String key = child.getKey();
                    String val = child.getValue(String.class);
                    SuperMarket ss = new SuperMarket();

                    if(key.equals("lat"))
                        ss.setLatitude(Double.parseDouble(val));

                    if(key.equals("lon"))
                        ss.setLongitude(Double.parseDouble(val));

                    if(key.equals("name"))
                        ss.setName(val);

                    s.add(ss);

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return s;

    }


}
